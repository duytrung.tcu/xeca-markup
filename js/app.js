/**
 * Init Third-party libs
 */
// eva.replace();
// AOS.init();

/**
 * App Utils
 */
const is = {
    arr: a => Array.isArray(a),
    obj: a => stringContains(Object.prototype.toString.call(a), 'Object'),
    pth: a => is.obj(a) && a.hasOwnProperty('totalLength'),
    svg: a => a instanceof SVGElement,
    inp: a => a instanceof HTMLInputElement,
    dom: a => a.nodeType || is.svg(a),
    str: a => typeof a === 'string',
    fnc: a => typeof a === 'function',
    und: a => typeof a === 'undefined',
    hex: a => /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(a),
    int: a => typeof a === 'number' && isFinite(a) && Math.floor(a) === a,
    empty: a => [Object, Array].includes((a || {}).constructor) && !Object.entries((a || {})).length
};

/**
 * Event Handlers
 */
const mainHeader = document.querySelector('.main-header');
const mobileNavTrigger = document.querySelector('#mobile-nav-trigger');
const mobileNavLinks = document.querySelectorAll('.mobile-nav__list > li');
const dropDownNavLinks = document.querySelectorAll('.mobile-nav__list > li a[aria-haspopup]');

dropDownNavLinks.forEach(link => {
    link.addEventListener('click', function () {
        link.setAttribute('aria-expanded', !(link.getAttribute('aria-expanded') === 'true'));
    });
});

// add animation delay to mobile nav links
mobileNavLinks.forEach((link, linkIdx) => {
    link.style.animationDelay = linkIdx * 50 /* delay gap */ + 'ms';
});

mobileNavTrigger.addEventListener('change', function (evt) {
    const isMobileNavOpen = evt.target.checked;
    document.body.classList.toggle('fixed');

    if (isMobileNavOpen) {
        mobileNavLinks.forEach(link => {
            animateCSS(link, 'fadeInUp');
        });
    }
});

function animateCSS(node, animationName, callback) {
    if (!node) return null;

    if (is.str(node)) {
        node = document.querySelector(node);
    }

    if (is.dom(node)) {
        node.classList.add('animated', animationName);
        node.addEventListener('animationend', handleAnimationEnd);
    }

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName);
        node.removeEventListener('animationend', handleAnimationEnd);
        if (is.fnc(callback)) callback();
    }
}

// Init navbar scroll effect
(function () {
    const headRoom = new Headroom(mainHeader, {
        tolerance: 5,
        offset: 120,
        classes: {
            initial: 'main-header',
            pinned: 'main-header--pinned',
            unpinned: 'main-header--unpinned',
        },
    });

    headRoom.init();
})();